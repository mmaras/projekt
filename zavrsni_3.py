import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()
#trazimo u koliko ponavljanja mozemo ocekivati uzastupno pojavljivanje broja 7
#ovaj kod bi bilo najbolje pokretati pomocu GPUa gdje mozemo koristiti mnogo vise thredova za izracun da bi nam primjer bio sto tocniji
n_iterations = 5000
hits = 0
for i in range(0,n_iterations):
    x = np.random.randint(0,36)
    y = np.random.randint(0,36)
    if x == 7 and y == 7:
        break
    else:
        hits +=1

part = np.zeros(1,dtype=np.float64)
start = time.time()
part[0] = hits

if rank == 0:
    rez = np.empty(1)
else:
    rez = None

comm.Reduce(part,rez,op=MPI.SUM,root=0)

if rank == 0:
    sansa = int(rez)/int(size)
    error = ((37+(1/37)+(36/37))*37)
    end = time.time()
    print("Ocekivano uzastupno pojavljivanje broja 7 pomocu Monte Carlo metode je u ",sansa ," vrtenja ruleta,"," dok je matematickom formulom to broj ",error)
    
#U vezi izracuna vremena, koristenjem vise procesa povecava se vrijeme izvodenja programa, sto je i logicno, testirao sam s 1,10,100,1000 procesa te najbolji rezultat dobio s 100 procesa,
#dok s 1000 procesa uopce nije zavrsilo izvodenje. Sto nam znaci da ne mozemo koristiti beskonacan broj procesa i ocekivati na taj nacin najbolji rezultat vec bi trebali na neki nacin poboljsati algoritam programa.


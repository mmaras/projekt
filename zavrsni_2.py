import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

n_iterations = 100000
hits = 0
a = 1.0/37.0
for i in range(0,n_iterations):
    x = np.random.uniform(0,36)
    y = np.random.uniform(0,36)
    z = np.random.uniform(0,36)
    if x < 1 and y < 1 and z < 1:
        hits += 1
        
part = np.zeros(1,dtype=np.float64)
part[0] = float(hits)/float(n_iterations)

if rank == 0:
    rez = np.empty(1)
else:
    rez = None

comm.Reduce(part,rez,op=MPI.SUM,root=0)

if rank == 0:
    sansa = rez/size
    error = abs(sansa - (a*a*a))
    print("Mogucnost ponavljanja istog broja 3 puta je %.16f, error je otprilike %.16f" %(sansa,error))
